package ru.tsc.korosteleva.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.korosteleva.tm.enumerated.Sort;
import ru.tsc.korosteleva.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    M add(@NotNull M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@NotNull Comparator<M> comparator);

    @NotNull
    List<M> findAll(@NotNull Sort sort);

    @Nullable
    M findOneById(@NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull Integer index);

    @Nullable
    M remove(@NotNull M model);

    @Nullable
    M removeById(@NotNull String id);

    @Nullable
    M removeByIndex(@NotNull Integer index);

    void removeAll(@NotNull Collection<M> collection);

    void clear();

    long getSize();

    boolean existsById(@NotNull String id);

}
