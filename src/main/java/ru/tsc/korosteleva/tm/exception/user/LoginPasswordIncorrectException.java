package ru.tsc.korosteleva.tm.exception.user;

public final class LoginPasswordIncorrectException extends AbstractUserException {

    public LoginPasswordIncorrectException() {
        super("Error! Incorrect login or password entered.");
    }

}
