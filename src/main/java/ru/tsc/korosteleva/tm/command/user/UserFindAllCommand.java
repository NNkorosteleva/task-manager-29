package ru.tsc.korosteleva.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.korosteleva.tm.enumerated.Role;
import ru.tsc.korosteleva.tm.model.User;

import java.util.List;

public class UserFindAllCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-find-all";

    @Nullable
    public static final String ARGUMENT = null;

    @NotNull
    public static final String DESCRIPTION = "Find all users.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[FIND ALL USERS]");
        final List<User> users = getUserService().findAll();
        renderUsers(users);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
