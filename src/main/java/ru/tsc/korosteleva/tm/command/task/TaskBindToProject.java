package ru.tsc.korosteleva.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.korosteleva.tm.util.TerminalUtil;

public class TaskBindToProject extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-bind-to-project";

    @Nullable
    public static final String ARGUMENT = null;

    @NotNull
    public static final String DESCRIPTION = "Bind task to project.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
        System.out.println("[OK]");
    }

}
